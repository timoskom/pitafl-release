# PITAFL - Release

**Official Releases of the PITAFL App**

# Pantry, Is There Any Food Left? (PITAFL)
## Pantry Manager
**PITAFL is an android app for pantry management & smart (food) shopping list creation.**
## Pronunciation
**paɪtɑːfl**
(GR: πάιταφλ)
